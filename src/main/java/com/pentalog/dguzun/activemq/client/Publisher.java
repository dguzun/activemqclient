package com.pentalog.dguzun.activemq.client;

import javax.jms.Destination;

import org.springframework.jms.core.JmsTemplate;

import com.pentalog.dguzun.activemq.client.vo.PersonVO;

public class Publisher {
	private JmsTemplate template;
	private Destination[] destinations;

	public void start(PersonVO personVO) {
		for (int i = 0; i < destinations.length; i++) {
			Destination destination = destinations[i];
			template.send(destination, new PersonMessageCreator(destination, personVO));
		}
	}

	public JmsTemplate getTemplate() {
		return template;
	}

	public void setTemplate(JmsTemplate template) {
		this.template = template;
	}

	public Destination[] getDestinations() {
		return destinations;
	}

	public void setDestinations(Destination[] destinations) {
		this.destinations = destinations;
	}

}