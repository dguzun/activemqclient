package com.pentalog.dguzun.activemq.client.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pentalog.dguzun.activemq.client.Publisher;
import com.pentalog.dguzun.activemq.client.vo.PersonVO;

@Controller
@RequestMapping(value = "/index")
public class PersonController {
	@Autowired
	private Publisher publisher;
	
	@RequestMapping(method=RequestMethod.GET)
    public String personForm(Model model) {
        model.addAttribute("person", new PersonVO());
        return "index";
    }

    @RequestMapping(value="/sendMessage", method=RequestMethod.POST)
    public String sendMeessage(@ModelAttribute("person") PersonVO personVO, Model model) {
        model.addAttribute("name", personVO.getName());
        model.addAttribute("age", personVO.getAge());
        model.addAttribute("isAdmin", personVO.getIsAdmin());
        
        this.publisher.start(personVO);
        
        return "index";
    } 
}
