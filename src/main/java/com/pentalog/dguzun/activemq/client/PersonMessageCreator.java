package com.pentalog.dguzun.activemq.client;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.activemq.command.ActiveMQMapMessage;
import org.springframework.jms.core.MessageCreator;

import com.pentalog.dguzun.activemq.client.vo.PersonVO;


public class PersonMessageCreator implements MessageCreator{
    private Destination destination;
    private PersonVO personVO;

    public PersonMessageCreator(Destination destination, PersonVO personVO) {
            this.destination = destination;
            this.personVO = personVO;
    }

    public Message createMessage(Session session) throws JMSException {
            MapMessage message = session.createMapMessage();
            message.setString("destination", this.destination.toString());
            message.setString("name", this.personVO.getName());
            message.setInt("age", this.personVO.getAge());
            message.setBoolean("isAdmin", this.personVO.getIsAdmin());
            System.out.println("Sending: " + ((ActiveMQMapMessage)message).getContentMap() + " on destination: " + destination);
            return message;
    }
}
