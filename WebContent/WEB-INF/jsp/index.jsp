<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Send a message to ActiveMQ</title>
</head>
<body>

<h2>Send a message to ActiveMQ</h2>
<form:form method="POST" commandName="person" action="/Client/index/sendMessage">
   <table>
    <tr>
        <td><form:label path="name">Name</form:label></td>
        <td><form:input path="name" /></td>
    </tr>
    <tr>
        <td><form:label path="age">Age</form:label></td>
        <td><form:input path="age" /></td>
    </tr>
    <tr>
        <td><form:label path="isAdmin">Is admin</form:label></td>
        <td><form:checkbox path="isAdmin" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Send"/>
        </td>
    </tr>
</table>  
</form:form>
</body>
</html>